#include <SoftwareSerial.h>
 
SoftwareSerial RFID = SoftwareSerial(4,5);

void setup()  
{
  Serial.begin(9600);
  RFID.begin(9600);
}
 
void loop(){
 
  while(RFID.available()>0)
  {
      int i = RFID.read();
      Serial.print(i, HEX);
      Serial.print(" ");
  }
}
