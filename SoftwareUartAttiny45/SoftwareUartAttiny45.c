#define F_CPU 8000000L

#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include "softuart.h"

#define LED_PIN 4
#define BUZZER_PIN 2
#define RFID_ID_LEN 14
#define RFID_BEGIN_BYTE 0x02
#define RFID_END_BYTE 0x03
#define RFID_INVALID_BYTE 0x04

// Valid ids(HEX)
// 2 32 32 30 30 45 34 45 39 44 31 46 45 3// 2 31 42 30 30 34 38 44 30 46 31 37 32 3
char sleep = 0;

unsigned validrfids[][RFID_ID_LEN] =	{ 
											{ RFID_BEGIN_BYTE, 0x31, 0x42, 0x30, 0x30, 0x33, 0x34, 0x41, 0x36, 0x31, 0x46, 0x39, 0x36, RFID_END_BYTE },
											{ RFID_BEGIN_BYTE, 0x31, 0x42, 0x30, 0x30, 0x34, 0x38, 0x36, 0x31, 0x32, 0x31, 0x31, 0x33, RFID_END_BYTE },
											{ RFID_BEGIN_BYTE, 0x31, 0x42, 0x30, 0x30, 0x33, 0x34, 0x38, 0x46, 0x39, 0x46, 0x33, 0x46, RFID_END_BYTE },
										};

int read_rfid_and_check()
{
	int identifierIdx = 0;
	int bytes[RFID_ID_LEN];
	int i = 0;
	int j = 0;
	int valid_id = 0;
	softuart_flush_input_buffer();
	while(1)
	{
		if (identifierIdx == RFID_ID_LEN)
		{
			break;
		}
		if (!softuart_kbhit())
		{
			continue;
		}
		bytes[identifierIdx++] = softuart_getchar();// - '0';
	}

	for (i = 0; i < sizeof(validrfids) / sizeof(validrfids[0]); ++i)
	{
		valid_id = 1;
		for (j = 0; j < RFID_ID_LEN; ++j)
		{
			valid_id &= validrfids[i][j] == bytes[j];
		}
		if (valid_id)
		{
			break;
		}
	}
	
	return valid_id;
}

ISR(TIM1_OVF_vect)
{
	TCNT1 = 12;
	sleep++;
	if (sleep >= 10)
	{
		TIMSK &= ~(1 << TOIE1);
		sleep = 0;
		PORTB &= ~(1 << BUZZER_PIN);
	}
}

int main(void)
{	
	softuart_init();
	softuart_turn_rx_on(); // on by default
	
	sei();
	DDRB |= (1 << LED_PIN) | (1 << BUZZER_PIN);
	PORTB |= (0 << LED_PIN) | (0 << BUZZER_PIN);
	TCCR1 &= ~((1 << COM1A1) | (1 << COM1A0));
	TCCR1 &= ~((1 << COM1B1) | (1 << COM1B0));
	TCCR1 |= (1 << CS13) | (1 << CS12);
	TCCR1 &= ~((1 << CS11) | (1 << CS00));

	for (;;) 
	{
		if (read_rfid_and_check()) 
		{
			if (!(TIMSK & 1 << TOIE1))
			{
				TIMSK |= (1<<TOIE1);
				TCNT1 = 0;
				PORTB ^= (1 << LED_PIN);
				PORTB |= (1 << BUZZER_PIN);
			}
		}
	}
	
	return 0;
}